<?php

/**
 * Display tickets
 */
function support_deadline_page($client = NULL, $state = NULL) {
  global $user;

  if (!$client) {
    if (isset($_SESSION['support_client']) && $client = support_client_load($_SESSION['support_client'])) {
      drupal_goto("support-deadline/$client->path");
    }
    $clients = _support_available_clients();
    if (sizeof($clients)) {
      foreach ($clients as $key => $name) {
        $client = support_client_load($key);
        drupal_goto("support-deadline/$client->path");
      }
    }
  }
  else {
    if (is_numeric($client)) {
      $_SESSION['support_client'] = $client;
    }
    else {
      drupal_set_message(t('Client does not exist or is not enabled.'), 'error');
      if (isset($_SESSION['support_client'])) {
        unset($_SESSION['support_client']);
      }
      drupal_goto('');
    }
  }

  $output = l(t('Post new support ticket'), 'node/add/support-ticket');

  drupal_add_css(drupal_get_path('module', 'support_deadline') .'/support-deadline.css');
  drupal_add_css(drupal_get_path('module', 'support') .'/support-tickets.css');

  if (!$state) {
    $state = 'all open';
  }
  $state = _support_get_state($state);

  $rows = array();
  $header = array(
    array('data' => t('Id'), 'field' => 'n.nid'),
    array('data' => t('Ticket'), 'field' => 'n.title'),
    array('data' => t('Updated'), 'field' => 'last_updated', 'sort' => 'desc'),
    //    array('data' => t('Reported by'), 'field' => 'n.uid'),
    array('data' => t('Assigned to'), 'field' => 't.assigned'),
    array('data' => t('State'), 'field' => 't.state'),
    array('data' => t('Priority'), 'field' => 't.priority'),
    array('data' => t('Due Date'), 'field' => 'deadline_order'),
    array('data' => t('Hours (Act/Est)'), ),
    //    array('data' => t('Updates'), 'field' => 'l.comment_count'),
  );
  if ($state == -2) {
    $my_open = " AND t.assigned = $user->uid";
  }
  else {
    $my_open = "";
  }

  if ($state < 0) {
    $state = 'AND t.state NOT IN ('. implode(', ', _support_get_state(SUPPORT_STATE_CLOSED)) .')';
  }
  else {
    $state = $state ? "AND t.state = $state" : '';
  }

  $sql = "SELECT DISTINCT(n.nid), n.title, n.type, n.changed, n.uid, u.name, GREATEST(n.changed, l.last_comment_timestamp) AS last_updated, l.comment_count, t.state, t.priority, dt.deadline, IF(dt.deadline > 0, dt.deadline, 99999999999) AS 'deadline_order', dt.hours_estimate, dt.hours_actual, t.assigned FROM {node} n LEFT JOIN {support_ticket} t ON n.nid = t.nid LEFT JOIN {support_deadline_ticket} dt ON n.nid = dt.nid INNER JOIN {node_comment_statistics} l ON n.nid = l.nid INNER JOIN {users} u ON n.uid = u.uid LEFT JOIN {comments} c ON n.nid = c.nid WHERE (c.status = ". COMMENT_PUBLISHED ." OR c.status IS NULL) AND n.status = 1 AND n.type = 'support_ticket' AND client = $client $state $my_open";
  if (!user_access('view other users tickets') && !user_access('administer support') && !user_access('edit any ticket') && !user_access('delete any ticket')) {
    $sql .= " AND n.uid = $user->uid";
  }
  $sql = db_rewrite_sql($sql);
  $sql .= tablesort_sql($header);
  switch (variable_get('support_secondary_sort_order', 0)) {
    case 0:
      $order = 'ASC';
      break;
    case 1:
      $order = 'DESC';
      break;
  }
  switch (variable_get('support_secondary_sort_tickets', 0)) {
    case 1:
      $sql .= ", last_updated $order";
      break;
    case 2:
      $sql .= ", n.nid $order";
      break;
    case 3:
      $sql .= ", t.state $order";
      break;
    case 4:
      $sql .= ", t.priority $order";
      break;
  }
  $sql_count = "SELECT COUNT(DISTINCT(n.nid)) FROM {node} n LEFT JOIN {comments} c ON n.nid = c.nid AND (c.status = %d OR c.status IS NULL) LEFT JOIN {support_ticket} t ON n.nid = t.nid WHERE n.status = 1 AND client = $client $state $my_open";
  $sql_count = db_rewrite_sql($sql_count);
  $result = pager_query($sql, 50, 0, $sql_count);
  $rows = array();
  $estimate_total = 0;
  $spent_total = 0;
  while ($ticket = db_fetch_object($result)) {
    $account = user_load(array('uid' => $ticket->uid));
    $assigned = user_load(array('uid' => $ticket->assigned));
    $comments = l($ticket->comment_count, "node/$ticket->nid", array('fragment' => 'comments'));
    if ($new = comment_num_new($ticket->nid)) {
      $node = node_load($ticket->nid);
      $comments .= '&nbsp;('. l(format_plural($new, '1 new', '@count new'), "node/$ticket->nid", array('query' => comment_new_page_count($node->comment_count, $new, $node), 'fragment' => 'new')) .')';
    }
    $rows[] = array('data' => array(
      array('data' => l($ticket->nid, "node/$ticket->nid"), 'class' => 'ticket-id'),
      array('data' => l(_support_truncate($ticket->title), "node/$ticket->nid"), 'class' => 'ticket-title'),
      array('data' => format_date($ticket->last_updated, 'small'), 'class' => 'ticket-updated'),
      //      array('data' => l(_support_truncate($account->name, 24), "user/$account->uid"), 'class' => 'ticket-reported'),
      array('data' => l(_support_truncate($assigned->name, 24), "user/$assigned->uid"), 'class' => 'ticket-assigned'),
      array('data' => _support_state($ticket->state), 'class' => 'ticket-state'),
      array('data' => _support_priorities($ticket->priority), 'class' => 'ticket-priority'),
      array('data' => $ticket->deadline ? date_format_date(date_convert($ticket->deadline, DATE_UNIX, DATE_OBJECT), 'custom', 'm/d/y') : '', 'class' => 'ticket-deadline'),
      array('data' => ($ticket->hours_actual || $ticket->hours_estimate ? $ticket->hours_actual .' / '. $ticket->hours_estimate : ''), 'class' => 'ticket-hours' ),

      /*      array('data' => $comments) */), 'class' => "state-$ticket->state priority-$ticket->priority", );
    $estimate_total += $ticket->hours_estimate;
    $spent_total += $ticket->hours_actual;
  }
  $rows[] = array(array('colspan' => 7, 'style' => 'text-align: right', 'data' => t('Total Hours (Spent / Estimated):')), "$spent_total / $estimate_total");

  $output .= theme('table', $header, $rows, array('class' => 'support')) . theme('pager');
  return $output;
}
